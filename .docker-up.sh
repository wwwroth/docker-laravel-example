cp .env.example .env
docker-compose run --rm --no-deps app-server composer install
docker-compose run --rm --no-deps app-server php artisan key:generate
docker-compose run --rm --no-deps app-server php artisan horizon:install
docker-compose run --rm --no-deps app-server php artisan telescope:install
docker-compose run --rm --no-deps app-server php artisan storage:link
docker run --rm -it -v $(pwd):/app -w /app node yarn
docker-compose up -d